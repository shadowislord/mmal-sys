#!/bin/sh

if [ "$#" -ne 2 ]; then
    echo "usage: $0 <rust version> <firmware version>"
    exit 1
fi

if [ "$(id -u)" != "0" ]; then
    echo "Please run as root"
    exit 1
fi

if [ ! -n "$RUSTUP_HOME" ]; then
    echo "RUSTUP_HOME must be defined"
    exit 1
fi

if [ ! -n "$CARGO_HOME" ]; then
    echo "CARGO_HOME must be defined"
    exit 1
fi

if [ ! -n "$FIRMWARE_HOME" ]; then
    echo "FIRMWARE_HOME must be defined"
    exit 1
fi

if [ -d "$RUSTUP_HOME" ]; then
    HAVE_RUST=y
fi

if [ -d "$FIRMWARE_HOME" ]; then
    HAVE_FIRMWARE=y
fi

if [ "$HAVE_RUST" = "y" ] && [ "$HAVE_FIRMWARE" = "y" ]; then
    exit 0
fi

RUST_VERSION="$1"
FIRMWARE_VERSION="1.$2"

apt-get update -yqq
apt-get install -yqq --no-install-recommends wget

if [ "$HAVE_RUST" != "y" ]; then
    wget "https://static.rust-lang.org/rustup/archive/1.18.3/x86_64-unknown-linux-gnu/rustup-init"
    chmod +x rustup-init
    ./rustup-init -y --no-modify-path --default-toolchain "$RUST_VERSION"
    rm rustup-init
fi

if [ "$HAVE_FIRMWARE" != "y" ]; then
    if [ ! -d "$FIRMWARE_HOME" ]; then
        mkdir "$FIRMWARE_HOME"
    fi

    wget -O - "https://github.com/raspberrypi/firmware/archive/$FIRMWARE_VERSION.tar.gz" \
        | tar -xzf - -C "$FIRMWARE_HOME" --strip-components 2 "firmware-$FIRMWARE_VERSION/hardfp/opt/vc"
fi
